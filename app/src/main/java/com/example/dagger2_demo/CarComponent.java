package com.example.dagger2_demo;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@PerActivity
@Component(modules = {WheelsModule.class, PetrolEngineModule.class})
public interface CarComponent {

    Car getCar();
    void inject(MainActivity mainActivity);

    @Component.Builder
    interface Builder{

        @BindsInstance
        Builder engineCapacity(@Named("engine capacity") int engineCapacity);

        @BindsInstance
        Builder horsePower(@Named("horse power") int horsePower);

        CarComponent build();
    }
}
