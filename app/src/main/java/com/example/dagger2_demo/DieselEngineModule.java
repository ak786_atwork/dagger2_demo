package com.example.dagger2_demo;

import javax.inject.Inject;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public  class DieselEngineModule {

    private int horsePower;

    @Provides
    int providesHorsePower(){
        return horsePower;
    }

    public DieselEngineModule(int horsePower) {
        this.horsePower = horsePower;
    }

    @Provides
    Engine providesEngine(DieselEngine engine){
        return engine;
    }

}
