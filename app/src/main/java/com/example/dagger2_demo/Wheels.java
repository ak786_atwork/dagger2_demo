package com.example.dagger2_demo;

class Wheels {

    //you are not owner of this class

    private Rims rims;
    private Tyres tyres;


    Wheels(Rims rims, Tyres tyres) {
        this.rims = rims;
        this.tyres = tyres;
    }
}
