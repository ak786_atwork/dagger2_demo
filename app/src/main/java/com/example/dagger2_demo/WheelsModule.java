package com.example.dagger2_demo;

import com.example.dagger2_demo.Rims;
import com.example.dagger2_demo.Tyres;
import com.example.dagger2_demo.Wheels;

import dagger.Module;
import dagger.Provides;

@Module
public  class WheelsModule {

    @Provides
    static Rims provideRims(){
        return new Rims();
    }

    @Provides
    static Tyres provideTyres(){
        Tyres tyres = new Tyres();
        tyres.inflate();
        return tyres;
    }

    @Provides
    static Wheels provideWheels(Rims rims, Tyres tyres){
        return new Wheels(rims, tyres);
    }

}
