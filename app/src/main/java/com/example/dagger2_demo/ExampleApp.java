package com.example.dagger2_demo;

import android.app.Application;

public class ExampleApp extends Application {

    private CarComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerCarComponent.builder()
                .engineCapacity(1400)
                .horsePower(100)
                .build();
    }

    public CarComponent getComponent() {
        return component;
    }
}
